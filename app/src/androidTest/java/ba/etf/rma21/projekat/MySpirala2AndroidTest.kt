//package ba.etf.rma21.projekat
//
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.NavigationViewActions
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import org.hamcrest.CoreMatchers
//import org.hamcrest.CoreMatchers.not
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun testPrvogZadatka() {
//        //dodajem jedan predmet na listu, a to je predmet koji ima kviz u skupini buducih!
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//        val nedodjeljeniKvizovi = KvizRepository.getAll().minus(KvizRepository.getMyKvizes())
//        val nedodjeljeniPredmeti = PredmetRepository.getAll().minus(PredmetRepository.getUpisani())
//
//        var grupaVrijednost = ""
//        var predmetNaziv = ""
//        var godinaVrijednost = -1
//        for (nk in nedodjeljeniKvizovi) {
//            for (np in nedodjeljeniPredmeti) {
//                if (nk.nazivPredmeta == np.naziv) {
//                    grupaVrijednost = nk.nazivGrupe
//                    godinaVrijednost = np.godina
//                    predmetNaziv = np.naziv
//
//                }
//            }
//        }
//
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(godinaVrijednost.toString())
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(predmetNaziv)
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(grupaVrijednost)
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//
//        //nakon sto smo dodali predmet provjeravamo da li je moguce kliknuti na njega u Svi moji kvizovi jer je medu buducim kvizovima.
//        onView(withId(R.id.kvizovi)).perform(click())
//        onView(withId(R.id.filterKvizova)).perform(click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Svi moji kvizovi"))).perform(click())
//        val kvizovi = KvizRepository.getFuture()
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(kvizovi[0].naziv)),
//                hasDescendant(withText(kvizovi[0].nazivPredmeta))), click())).check(matches(not(isClickable())))
//
//        //nakon sto smo dodali predmet provjeravamo da li je moguce kliknuti na bilo koji kviz u Svi kvizovi.
//        onView(withId(R.id.filterKvizova)).perform(click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Svi kvizovi"))).perform(click())
//        val kvizovi2 = KvizRepository.getMyKvizes()
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(kvizovi2[0].naziv)),
//                hasDescendant(withText(kvizovi2[0].nazivPredmeta))), click())).check(matches(not(isClickable())))
//
//        //provjeravamo da li je moguce kliknuti na neki kviz u Buduci kvizovi.
//        onView(withId(R.id.filterKvizova)).perform(click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Budući kvizovi"))).perform(click())
//        val kvizovi1 = KvizRepository.getFuture()
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(kvizovi1[0].naziv)),
//                hasDescendant(withText(kvizovi1[0].nazivPredmeta))), click())).check(matches(not(isClickable())))
//
//        //provjeravamo je li zapamceno ono sto je posljednji put odabrano
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("2")
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("DM")
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("DMG2")
//                )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.kvizovi)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).check(matches(withSpinnerText("2")))
//        Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).check(matches(withSpinnerText("DM")))
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).check(matches(withSpinnerText("DMG2")))
//        }
//
//    @Test
//    fun testDrugogZadatka() {
//        //predam kviz i provjerim da li mi je ispravno u navigation postavljeno
//        onView(withId(R.id.predmeti)).perform(click())
//        onView(withId(R.id.dodajPredmetDugme)).perform(click())
//        onView(withId(R.id.kvizovi)).perform(click())
//        onView(withId(R.id.filterKvizova)).perform(click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Svi moji kvizovi"))).perform(click())
//        val kvizovi = KvizRepository.getMyKvizes()
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(kvizovi[1].naziv)),
//                hasDescendant(withText(kvizovi[1].nazivPredmeta))), click()))
//        val pitanja = PitanjeKvizRepository.getPitanja(kvizovi[1].naziv, kvizovi[1].nazivPredmeta)
//        var indeks = 0
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(indeks))
//        onView(withId(R.id.tekstPitanja)).check(matches(withText(pitanja[indeks].tekstPitanja)))
//
//        onView(withId(R.id.zaustaviKviz)).perform(click())
//        onView(withId(R.id.predajKviz)).check(matches(not(isDisplayed())))
//        onView(withId(R.id.zaustaviKviz)).check(matches(not(isDisplayed())))
//        onView(withId(R.id.kvizovi)).check(matches(isDisplayed()))
//        onView(withId(R.id.predmeti)).check(matches(isDisplayed()))
//        onView(withId(R.id.filterKvizova)).check(matches(isDisplayed()))
//
//        onView(withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(kvizovi[1].naziv)),
//                hasDescendant(withText(kvizovi[1].nazivPredmeta))), click()))
//
//        onView(withId(R.id.predajKviz)).perform(click())
//        onView(withSubstring("Završili ste kviz")).check(matches(isDisplayed()))
//        onView(withId(R.id.predajKviz)).check(matches(not(isDisplayed())))
//        onView(withId(R.id.zaustaviKviz)).check(matches(not(isDisplayed())))
//        onView(withId(R.id.kvizovi)).check(matches(isDisplayed()))
//        onView(withId(R.id.predmeti)).check(matches(isDisplayed()))
//        onView(withText("Rezultat")).check(matches(isDisplayed()))
//        onView(withId(R.id.navigacijaPitanja)).perform(NavigationViewActions.navigateTo(0))
//        onView(withText("Rezultat")).perform(click())
//        onView(withSubstring("Završili ste kviz")).check(matches(isDisplayed()))
//    }
//
//    }