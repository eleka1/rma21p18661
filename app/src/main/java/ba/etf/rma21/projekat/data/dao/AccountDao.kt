package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDao {

    @Query("SELECT * FROM account")
    suspend fun getAllAccount(): List<Account>

    @Query("DELETE FROM account")
    suspend fun ocistiAccount()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAccount(vararg account: Account) //ubacuje Account
}