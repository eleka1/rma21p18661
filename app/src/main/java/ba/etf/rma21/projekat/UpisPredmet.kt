//package ba.etf.rma21.projekat
//
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.view.View
//import android.widget.*
//import androidx.appcompat.app.AppCompatActivity
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import java.util.stream.Collectors
//
//
//class UpisPredmet : AppCompatActivity() {
//
//    private lateinit var godina: Spinner
//    private lateinit var predmet : Spinner
//    private lateinit var grupa : Spinner
//    private lateinit var dugmeUpis : Button
//    private lateinit var kvizViewModel : KvizListViewModel
//    private lateinit var predmetViewModel : PredmetViewModel
//    private lateinit var grupaViewModel : GrupaViewModel
//    var odabranaGodina : Int = 1
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.upis_predmet)
//
//        kvizViewModel = KvizListViewModel(null,null)
//        predmetViewModel = PredmetViewModel(null, null)
//        grupaViewModel = GrupaViewModel()
//
//        godina = findViewById(R.id.odabirGodina)
//        predmet = findViewById(R.id.odabirPredmet)
//        grupa = findViewById(R.id.odabirGrupa)
//        dugmeUpis = findViewById(R.id.dodajPredmetDugme)
//
//        //preuzimanje podatka o godini koja je bila posljednja odabrana
//        odabranaGodina = intent.getIntExtra("odabranaGodinaMain", 0)
//
//        pozoviGodinu() //ovo poziva spiner za godinu
//
//        dugmeUpis.setOnClickListener(){
//            var nazivPredmeta: String = predmet.selectedItem.toString()
//            var nazivGrupe: String = grupa.selectedItem.toString()
//            //izmjena iz getAll u getAllKvizes
//            var dodaniKvizovi = kvizViewModel.getAllKvizes().filter { k: Kviz ->  k.nazivPredmeta.equals(nazivPredmeta) && k.nazivGrupe.equals(nazivGrupe)}
//            kvizViewModel.dodajMojKviz(dodaniKvizovi)
//            var dodaniPredmeti = predmetViewModel.getAll().filter { p:Predmet -> p.naziv.equals(nazivPredmeta)}
//            predmetViewModel.dodajMojPredmet(dodaniPredmeti)
//            var dodanaGrupa = grupaViewModel.getGroupsByPredmet(nazivPredmeta).filter { g: Grupa -> g.naziv.equals(nazivGrupe) }
//            grupaViewModel.dodajMojeGrupe(dodanaGrupa)
//
//            odabranaGodina = godina.selectedItemPosition
//
//            val resultIntent = Intent()
//            resultIntent.putExtra("odabranaGodina", odabranaGodina)
//            setResult(Activity.RESULT_OK, resultIntent)
//            finish()
//        }
//    }
//
//    fun pozoviGodinu(){
//        var godine = resources.getStringArray(R.array.godine)
//        var spinerAdapter: ArrayAdapter<String> = ArrayAdapter(this,android.R.layout.simple_list_item_1, godine)
//        spinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        godina.adapter = spinerAdapter
//        godina.setSelection(odabranaGodina)
//        godina.onItemSelectedListener = object :
//            AdapterView.OnItemSelectedListener
//        {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//
//                pozoviPredmete(position+1) //moram ovako jer pozicije idu od 0!!!
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {
//                // write code to perform some action
//            }
//        }
//    }
//
//    fun pozoviPredmete(godinaBroj: Int){
//        var predmetiNaziv : List<String>
//        predmetiNaziv = predmetViewModel.getAll().stream()
//            .filter( {p: Predmet -> p.godina == godinaBroj && predmetViewModel.getUpisani().filter { q: Predmet -> q.naziv.equals(p.naziv) }.isEmpty()})
//            .map { p: Predmet -> p.naziv }.collect(Collectors.toList())
//
//        var izabraniPredmet: String = ""
//        var spinerPredmetiAdapter: ArrayAdapter<String> = ArrayAdapter(this,android.R.layout.simple_list_item_1, predmetiNaziv)
//        spinerPredmetiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        predmet.adapter = spinerPredmetiAdapter
//        predmet.onItemSelectedListener = object :
//            AdapterView.OnItemSelectedListener
//        {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                izabraniPredmet = predmetiNaziv[position]
//                pozoviGrupe(izabraniPredmet)
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {
//                pozoviGrupe("")
//                // write code to perform some action
//            }
//        }
//        if(izabraniPredmet.isEmpty() || izabraniPredmet=="") {
//            pozoviGrupe("")
//        }
//    }
//
//    fun pozoviGrupe(nazivPredmeta: String){
//
////        val grupeNaziv = mutableListOf<String>(" ") //dodajem prazan string jer ne znam drugacije da popravim bug sa brojacima :)
////        grupeNaziv.addAll(grupaViewModel.getGroupsByPredmet(nazivPredmeta).map{ g: Grupa -> g.naziv})
//
//        var grupeNaziv = grupaViewModel.getGroupsByPredmet(nazivPredmeta).map{ g: Grupa -> g.naziv}
//
//
//        var spinerGrupeAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1, grupeNaziv)
//        spinerGrupeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        grupa.adapter = spinerGrupeAdapter
//        grupa.onItemSelectedListener = object :
//            AdapterView.OnItemSelectedListener
//        {
//            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
////                dugmeUpis.isEnabled = position != 0 && !grupeNaziv.isEmpty()
//                dugmeUpis.isEnabled = !grupeNaziv.isEmpty()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {
//                // write code to perform some action
//            }
//        }
//    }
//}