package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel(){
    val selected = MutableLiveData<String>()

    fun selectedItem(item: String) {
        selected.value = item
    }

    var predan: Boolean = false
    var kviz :  String
    var odabranaGrupaId : Int = 0
    var odabranaGodina : Int
    var odabraniPredmet : Int
    var odabranaGrupa : Int
    var kvizId : Int = 0
    var kvizTakenId : Int = 0
    var postotak : Int = 0
    var brojPitanja : Int = 1

    init {
        kviz = ""
        odabranaGodina = 0
        odabraniPredmet = 0
        odabranaGrupa = 0
    }

}