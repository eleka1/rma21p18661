package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.staticData.getAllPredmeti
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext



class PredmetIGrupaRepository {

    companion object{

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPredmeti():List<Predmet>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmeti()

                return@withContext response
            }
        }

        suspend fun getGrupe():List<Grupa>{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllGroups()

                return@withContext response
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa>{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupeZaPredmet(idPredmeta)

                return@withContext response
            }
        }

        suspend fun getMyPredmeti():List<Predmet>?{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var grupe = ApiAdapter.retrofit.getGrupeZaStudenta(hash)
                var response = mutableListOf<Predmet>()
                for(grupa in grupe) {
                    var predmet = ApiAdapter.retrofit.getPredmetZaId(grupa.predmetId)
                    if(!response.contains(predmet))
                        response.add(predmet)
                }
                return@withContext response
            }
        }

        suspend fun getPredmetiZaGodinu(godina: Int):List<Predmet>?{
            return withContext(Dispatchers.IO) {
                var sviPredmeti = getPredmeti()
                var mojiPredmeti = getMyPredmeti()
                var response = mutableListOf<Predmet>()
                if(mojiPredmeti == null)
                    response = sviPredmeti as MutableList<Predmet> //pazi mozda na ovo krahira
                if (sviPredmeti != null) {
                    for(predmet in sviPredmeti) {
                        if((mojiPredmeti!=null && !mojiPredmeti.contains(predmet)) && predmet.godina == godina)
                            response.add(predmet)
                    }
                }
                return@withContext response
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.upisiStudentaUGrupu(idGrupa, hash)
                var povrat = response.message.subSequence(0, 9).equals("Ne postoji") //popraviti ako treba //nemam pojma radi li
                return@withContext povrat
            }
        }

        suspend fun getUpisaneGrupe():List<Grupa>{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getGrupeZaStudenta(hash)

                return@withContext response
            }
        }
    }
}