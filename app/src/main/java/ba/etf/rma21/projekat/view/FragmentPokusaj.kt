package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.*
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class FragmentPokusaj(val pitanja: List<Pitanje>) : Fragment() {

    private lateinit var navigacija : NavigationView
    private lateinit var pitanjeViewModel : PitanjeKvizViewModel
    private  lateinit var kvizViewModel: KvizListViewModel
    private var kvizTakenViewModel = TakenKvizViewModel()
    private var odgovorViewModel = OdgovorViewModel()
    private lateinit var sharedViewModel : SharedViewModel
    private lateinit var meni : Menu
    private var brojac = 0
    private var position = 0 //pozicija pitanja
    private var tacni = 0 //borj tacnih odgovora
    private var procenat = 0 //procenat tacnosti kviza
    private var predan = false //da li je kviz predan

    private lateinit var viewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (activity as MainActivity).sharedViewModel
        kvizViewModel = (activity as MainActivity).kvizViewModel
        sharedViewModel = (activity as MainActivity).sharedViewModel

        sharedViewModel.brojPitanja = pitanja.size

        postaviSve()
    }

    fun postaviSve(){
            viewModel.selected.observe(this, Observer<String> { item ->
                if (navigacija.menu.size() != 0) {
                var menuItem = navigacija.menu.getItem(position)
                var naziv = menuItem.title
                val s = SpannableString(naziv)
                s.setSpan(ForegroundColorSpan(Color.parseColor(item)), 0, s.length, 0)
                menuItem.title = s
                }
            })
    }

    //listener za navigaciju
    private val pitanjaOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener{ item ->
        when (item.title) {
            item.title -> {
                if(item.title.toString() == "Rezultat"){
                    val porukaFragment = FragmentPoruka.newInstance()
                    val args = Bundle()
                    procenat = sharedViewModel.postotak
                    args.putString("poruka", "Završili ste kviz ${viewModel.kviz} sa tačnosti ${sharedViewModel.postotak}%")
                    porukaFragment.setArguments(args)
                    openFragment(porukaFragment)
                }else {
                    position = item.title.toString().toInt() - 1
                    val pitanjeFragment = FragmentPitanje.newInstance(pitanja[position])

                    val bundle = Bundle()
                    bundle.apply {
                        putBoolean("predan", predan)
                    }
                    pitanjeFragment.arguments = bundle
                    openFragment(pitanjeFragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
        }
        false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.pokusaj_fragment, container, false)
        navigacija = view.findViewById(R.id.navigacijaPitanja)

        //postavljam ispravnu vidljivost za navigaciju
        var aktivnost = activity as MainActivity
        meni = aktivnost.getBottomNavigation().menu
        meni.findItem(R.id.kvizovi).isVisible = false
        meni.findItem(R.id.predmeti).isVisible = false
        meni.findItem(R.id.predajKviz).isVisible = true
        meni.findItem(R.id.zaustaviKviz).isVisible = true


        kvizTakenViewModel.zapocniKviz(
                onSuccess = ::onSuccessTK,
                onError = ::onError,
                id = sharedViewModel.kvizId
        )

        //listener za klik na zaustavi
        val zaustaviItemClickListener = MenuItem.OnMenuItemClickListener {
            viewModel.selectedItem("#FFFFFF")
                val kvizFragment = FragmentKvizovi.newInstance()
                openFragmentContainer(kvizFragment)
            return@OnMenuItemClickListener true
        }
        aktivnost.getBottomNavigation().menu.findItem(R.id.zaustaviKviz).setOnMenuItemClickListener(zaustaviItemClickListener)

        //listener za klik na predaj
        val predajItemClickListener = MenuItem.OnMenuItemClickListener {
            klikNaPredaj()
            return@OnMenuItemClickListener true
        }
        aktivnost.getBottomNavigation().menu.findItem(R.id.predajKviz).setOnMenuItemClickListener(predajItemClickListener)

        navigacija.setNavigationItemSelectedListener(pitanjaOnNavigationItemSelectedListener)
        return view;
    }

    companion object {
        fun newInstance(pitanja: List<Pitanje>): FragmentPokusaj = FragmentPokusaj(pitanja)
    }

    private fun postaviNavigationPitanja(odgovori: List<Odgovor>){
        pitanjeViewModel = PitanjeKvizViewModel()
        val menu = navigacija.menu

        while (brojac <= pitanja.size) {
            brojac++
            val item = menu.add(0,brojac-1, brojac-1, brojac.toString())
            if(brojac == pitanja.size+1){
                item.title = "Rezultat"
                if(!predan)
                    item.isVisible = false
                else
                    odgovorViewModel.dajBrojBodova(
                            onSuccess = ::onSuccessBodovi,
                            onError = ::onError,
                            kvizId = sharedViewModel.kvizId,
                            idKvizTaken = sharedViewModel.kvizTakenId
                    )
                continue
            }
            var odgovor = -1

            for(odgovoreni in odgovori){
                if(pitanja[brojac-1].idBaza == odgovoreni.pitanjeId) //ispravka
                    odgovor = odgovoreni.odgovoreno
            }

            var tacan = pitanja[brojac-1].tacan
            var boja = "#FFFFFF"

            if(odgovor != -1){
                if(odgovor == tacan)
                    boja = "#3DDC84"
                else
                    boja = "#DB4F3D"
            }

            var naziv = item.title
            val s = SpannableString(naziv)
            s.setSpan(ForegroundColorSpan(Color.parseColor(boja)), 0, s.length, 0)
            item.title = s
        }
    }

    private fun klikNaPredaj(){
        navigacija.menu.findItem(pitanja.size).isVisible = true

        context?.let {
            odgovorViewModel.dajOdgovore( //izmjena
                    it, onSuccess = ::onSuccessPredaj,
                    onError = ::onError,
                    id = sharedViewModel.kvizId
            )
        }

        meni.findItem(R.id.kvizovi).isVisible = true
        meni.findItem(R.id.predmeti).isVisible = true
        meni.findItem(R.id.predajKviz).isVisible = false
        meni.findItem(R.id.zaustaviKviz).isVisible = false

//        viewModel.selectedItem("#FFFFFF")


    }

    private fun openFragment(fragment: Fragment) {
        var aktivnost = activity as MainActivity
        val transaction = aktivnost.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.framePitanje, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    //ovo je drugi fragment koji se otvara u containeru a ne u framePitanje
    private fun openFragmentContainer(fragment: Fragment) {
        var aktivnost = activity as MainActivity
        val transaction = aktivnost.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun onSuccessBodovi(postotak: Int){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                sharedViewModel.postotak = postotak
                procenat = postotak
            }
        }
    }

    fun onSuccessTK(taken: KvizTaken){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                sharedViewModel.kvizTakenId = taken.id
                context?.let {
                    odgovorViewModel.dajOdgovore(
                            it, onSuccess = ::onSuccess,
                            onError = ::onError,
//                        id = taken.id
                            id = sharedViewModel.kvizId
                    )
                }
            }
        }
    }

    fun onSuccess(odgovori: List<Odgovor>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                //promjena
                if(sharedViewModel.brojPitanja == odgovori.size)
                    predan = true
                if(sharedViewModel.predan)
                    predan = true

                if(predan){
                    meni.findItem(R.id.kvizovi).isVisible = true
                    meni.findItem(R.id.predmeti).isVisible = true
                    meni.findItem(R.id.predajKviz).isVisible = false
                    meni.findItem(R.id.zaustaviKviz).isVisible = false
                }
                postaviNavigationPitanja(odgovori)
                postaviSve()

                val pitanjeFragment = FragmentPitanje.newInstance(pitanja[0])
                val bundle = Bundle()
                bundle.apply {
                    putBoolean("predan", predan)
                }
                pitanjeFragment.arguments = bundle

                openFragment(pitanjeFragment)

            }
        }
    }

    fun onSuccessPredaj(odgovori: List<Odgovor>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                for(pitanje in pitanja) {
                    var ima = false
                    for (odgovor in odgovori) {
                        if(pitanje.idBaza == odgovor.pitanjeId) //izmjena
                            ima = true
                    }
                    if(!ima){
                        context?.let {
                            odgovorViewModel.postaviOdgovor(
                                    it, onSuccess = ::onSuccessPostaviOdgovor,
                                    onError = ::onError,
                                    idKvizTaken = sharedViewModel.kvizTakenId,
                                    idPitanje = pitanje.idBaza, //izmjena
                                    odgovor = -2 //izmjena //-2 da mi se razlikuje od svake oznake to je neodgovoreno a predano
                            )
                        }
                    }
                }
                context?.let {
                    odgovorViewModel.dajOdgovore(
                            it, onSuccess = ::onSuccessDajOdgovorKraj,
                            onError = ::onError,
                            id = sharedViewModel.kvizId
                    )
                }
                val porukaFragment = FragmentPoruka.newInstance()
                val args = Bundle()
                args.putString("poruka", "Završili ste kviz ${viewModel.kviz} sa tačnosti ${sharedViewModel.postotak}%")
                porukaFragment.setArguments(args)
                openFragment(porukaFragment)
            }
        }
    }

    fun onSuccessDajOdgovorKraj(odgovori: List<Odgovor>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                for(o in odgovori) {
                    if(pitanja.filter { p -> p.idBaza==o.pitanjeId }.firstOrNull() != null)
                context?.let {
                    odgovorViewModel.predajOdgovore(it, onSuccess = ::onSuccessPredajOdgovor,
                            onError = ::onError,
                            id = o.kvizId) //izmjena moje gluposti bilo o.id
                }
                    break
                }
            }
        }
    }

    fun onSuccessPredajOdgovor(){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                //dodaj ako sta treba
            }
        }
    }

    fun onSuccessPostaviOdgovor(postotak: Int){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                sharedViewModel.postotak = postotak
            }
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Greska pri pozivanju", Toast.LENGTH_SHORT)
        toast.show()
    }

}