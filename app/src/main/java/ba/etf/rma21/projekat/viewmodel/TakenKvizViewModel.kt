package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TakenKvizViewModel{

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun zapocniKviz( onSuccess: (pokusaj: KvizTaken) -> Unit,
                onError: () -> Unit,
                id: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = TakeKvizRepository.zapocniKviz(id)

            // Display result of the network request to the user
            when (result) {
                is KvizTaken -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }
}