package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel{

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun postaviHash(context: Context, onSuccess: (tacno: Boolean) -> Unit,
                    onError: () -> Unit, playload: String) {
        // Create a new coroutine on the UI thread
        scope.launch {
            // Make the network call and suspend execution until it finishes
            AccountRepository.setContext(context)
            val result = AccountRepository.postaviHash(playload)

            // Display result of the network request to the user
            when (result) {
                is Boolean -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }

}