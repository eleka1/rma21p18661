package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjeKvizViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)


    fun getPitanjaZaKviz(context: Context, onSuccess: (grupe: List<Pitanje>) -> Unit,
                          onError: () -> Unit, kviz: Int){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            PitanjeKvizRepository.setContext(context)
            val result = PitanjeKvizRepository.getPitanja(kviz)

            // Display result of the network request to the user
            when (result) {
                is List<Pitanje> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

//    fun getOdgovor(pitanje : Pitanje) : Int{
//        return PitanjeKvizRepository.getPitanjeKviz(pitanje.naziv).odgovor
//    }
//
//    fun setOdgovor(pitanje : Pitanje, odgovor : Int){
//        PitanjeKvizRepository.setPitanjeKviz(pitanje.naziv, odgovor)
//    }
}