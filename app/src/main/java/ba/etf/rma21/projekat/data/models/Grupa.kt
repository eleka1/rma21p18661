package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
        @ColumnInfo(name = "PredmetId") @SerializedName("PredmetId") var predmetId: Int = 0 //pazi na ovo stavila si inicijalno 0!!! mozda bude problem
//        var nazivPredmeta: String = ""
) {
}