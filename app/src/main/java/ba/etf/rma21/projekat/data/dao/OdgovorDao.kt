package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor


@Dao
interface OdgovorDao{

    @Query("SELECT * FROM odgovor")
    suspend fun getAllOdgovor(): List<Odgovor>

    @Query("DELETE FROM odgovor")
    suspend fun ocistiOdgovor()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOdgovor(vararg odgovor: Odgovor)
}