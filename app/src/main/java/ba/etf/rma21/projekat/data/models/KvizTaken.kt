package ba.etf.rma21.projekat.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class KvizTaken(
        @SerializedName("id") val id: Int,
        @SerializedName("student") var student: String,
        @SerializedName("osvojeniBodovi") val osvojeniBodovi: Int,

        @SerializedName("datumRada") val datumRada: String,
        @SerializedName("KvizId") var KvizId: Int = 0 //prepravka
) {

}