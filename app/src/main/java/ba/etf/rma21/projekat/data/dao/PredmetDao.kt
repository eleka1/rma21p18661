package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDao {
    @Query("SELECT * FROM predmet")
    suspend fun getAllPredmeti(): List<Predmet>

    @Query("DELETE FROM predmet")
    suspend fun ocistiPredmet()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPredmet(vararg predmet: Predmet)
}