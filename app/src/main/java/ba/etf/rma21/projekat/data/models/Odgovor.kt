package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


//da li trebaju skriveni
@Entity
data class Odgovor(
        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "odgovoreno") @SerializedName("odgovoreno") var odgovoreno: Int,
        @SerializedName("KvizTakenId") var kvizTakenid: Int,
        @SerializedName("PitanjeId") var pitanjeId: Int,
        @ColumnInfo(name = "kvizId") var kvizId: Int //izmjena
) {
}