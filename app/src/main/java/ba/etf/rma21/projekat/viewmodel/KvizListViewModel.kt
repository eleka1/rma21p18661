package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*


class KvizListViewModel  {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getMyKvizes(context: Context, onSuccess: (movies: List<Kviz>) -> Unit,
                    onError: () -> Unit) {
        // Create a new coroutine on the UI thread
        scope.launch {
            // Make the network call and suspend execution until it finishes
            KvizRepository.setContext(context)
            val result = KvizRepository.getMyKvizes()

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else -> onError?.invoke()
            }
        }
    }


    fun getDone( onSuccess: (movies: List<Kviz>) -> Unit,
                 onError: () -> Unit){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = KvizRepository.getDone()

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun getFuture( onSuccess: (movies: List<Kviz>) -> Unit,
                   onError: () -> Unit){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = KvizRepository.getFuture()

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun getNotTaken( onSuccess: (movies: List<Kviz>) -> Unit,
                     onError: () -> Unit){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = KvizRepository.getNotTaken()

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun getAll( onSuccess: (movies: List<Kviz>) -> Unit,
                onError: () -> Unit){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = KvizRepository.getAll()

            // Display result of the network request to the user
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }
}