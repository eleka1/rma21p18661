package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PredmetViewModel{

    val scope = CoroutineScope(Job() + Dispatchers.Main)
    //dodani
    fun dodajMojPredmet(predmeti: List<Predmet>){
        PredmetRepository.dodajMojPredmet(predmeti)
    }

    fun getUpisani(): List<Predmet>{
        return PredmetRepository.getUpisani()
    }
    fun getAll(onSuccess: (movies: List<Predmet>) -> Unit,
               onError: () -> Unit){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = PredmetIGrupaRepository.getPredmeti()

            // Display result of the network request to the user
            when (result) {
                is List<Predmet> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }
    fun getPredmetiZaGodinu(onSuccess: (movies: List<Predmet>) -> Unit,
               onError: () -> Unit, godina: Int){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = PredmetIGrupaRepository.getPredmetiZaGodinu(godina)

            // Display result of the network request to the user
            when (result) {
                is List<Predmet> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

}