package ba.etf.rma21.projekat.data.models

import retrofit2.http.*
import java.util.*

interface Api {

    //najnovije dodano
//    @GET("/account/{id}/lastUpdate")
//    suspend fun getUpdateTF(@Path("id") id:String, @Query("date") datum:String): Changed

    @GET("/account/{id}/lastUpdate?=date")
    suspend fun getUpdateTF(@Path("id") id:String, @Query("date") date:String): Changed

    //za kvizove
    @GET("/kviz")
    suspend fun getAllKvizes(): List<Kviz>

    @GET("/kviz/{id}")
    suspend fun getKvizZaId(@Path("id") groupId:Int): Kviz

    @GET("/grupa/{id}/kvizovi")
    suspend fun getKvizoveZaIdGrupe(@Path("id") groupId:Int): List<Kviz>

    //za predmete
    @GET("/predmet/{id}")
    suspend fun getPredmetZaId(@Path("id") groupId:Int): Predmet

    @GET("/predmet")
    suspend fun getAllPredmeti(): List<Predmet>

    //za odgovor
    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovor(@Path("id") studentId:String, @Path("ktid") kvizId:Int): List<Odgovor>

    //dodaj POST zahtjev
    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun setOdgovor(@Path("id") studentId:String, @Path("ktid") kvizId:Int, @Body odgovor: OdgovorenoPitanje): Odgovor
    //za studenta
    @GET("/student/{id}/kviztaken")
    suspend fun getTakenKvizes(@Path("id") hashId:String): List<KvizTaken>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun zapocniKviz(@Path("id") studentId: String, @Path("kid") kvizId: Int): KvizTaken

    //za pitanje
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanjaZaKviz(@Path("id") kvizId:Int): List<Pitanje>

    //za grupe
    @GET("/grupa")
    suspend fun getAllGroups(): List<Grupa>

    @GET("/grupa/{id}")
    suspend fun getGrupaZaId(@Path("id") groupId:Int): Grupa

    @GET("/predmet/{id}/grupa")
    suspend fun getGrupeZaPredmet(@Path("id") groupId:Int): List<Grupa>

    @GET("/kviz/{id}/grupa")
    suspend fun getGrupaZaKviz(@Path("id") groupId:Int): List<Grupa>

    @GET("/student/{id}/grupa")
    suspend fun getGrupeZaStudenta(@Path("id") studentId: String): List<Grupa>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiStudentaUGrupu(@Path("gid") grupaId: Int, @Path("id") studentId: String): StringPovrat
}