package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.OdgovorViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentPitanje(val pitanje: Pitanje) : Fragment() {
    private lateinit var tekstPitanja : TextView
    private lateinit var listaOdgovora : ListView
    private var pitanjaKvizViewModel = PitanjeKvizViewModel()
    private var odgovorViewModel = OdgovorViewModel()
    private lateinit var sharedViewModel: SharedViewModel
    var predan = false //da li je kviz predan (ovo mi treba zbog navigationa)
    var odgovoreno = false

    private lateinit var viewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (activity as MainActivity).sharedViewModel
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.pitanje_fragment, container, false)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        tekstPitanja.text = pitanje.tekstPitanja
        listaOdgovora = view.findViewById(R.id.odgovoriLista)
        var odgovori : List<String> = pitanje.opcije
        var adapter = ArrayAdapter(inflater.context, android.R.layout.simple_list_item_1, odgovori)
        listaOdgovora.adapter=adapter
        listaOdgovora.onItemClickListener = object :
            AdapterView.OnItemClickListener
        {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                var tacan = listaOdgovora[pitanje.tacan] as TextView
                var odabrani = listaOdgovora[position] as TextView
                tacan.setBackgroundColor(Color.parseColor("#3DDC84"))
                if(position != pitanje.tacan) {
                    odabrani.setBackgroundColor(Color.parseColor("#DB4F3D"))
                    viewModel.selectedItem("#DB4F3D")
                }else{
                    viewModel.selectedItem("#3DDC84")
                }

                listaOdgovora.isEnabled = false //ne smije biti clickable kasnije
                if(!odgovoreno) {
                    context?.let {
                        odgovorViewModel.postaviOdgovor(
                                it, onSuccess = ::onSuccess,
                                onError = ::onError,
                                idKvizTaken = sharedViewModel.kvizTakenId,
                                idPitanje = pitanje.idBaza, //izmjena
                                odgovor = position
                        )
                    }
                }

            }
            }
        var bundle = this.arguments
        if(bundle != null)
            predan = bundle.getBoolean("predan") //preuzimam iz bundle da li je kviz predan

        return view;
    }

    // ova funkcija se poziva nakon onCreateView kada se kreira i onda dodatne postavke radimo
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        odgovorViewModel.dajOdgovore(
//                onSuccess = ::onSuccessO,
//                onError = ::onError,
//                id = sharedViewModel.kvizId
//        )
        context?.let {
            odgovorViewModel.dajOdgovore(
                    it,
                    onSuccess = ::onSuccessO,
                    onError = ::onError,
                    id = sharedViewModel.kvizId)
        }
    }

    companion object {
        fun newInstance(pitanje: Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
    }

    fun onSuccess(postotak: Int){ //mozda treba izmijeniti ovo int
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                sharedViewModel.postotak = postotak
            }
        }
    }

    fun onSuccessO(odgovori: List<Odgovor>){ //mozda treba izmijeniti ovo int
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
//                var nevalidno = sharedViewModel.brojPitanja
                var odgovor = -1
                for (odgovoreni in odgovori) {
                    if (odgovoreni.pitanjeId == pitanje.idBaza && odgovoreni.kvizId == pitanje.idKviza) //izmjena
                        odgovor = odgovoreni.odgovoreno //ovdje moze bilo sta validno doci ili -2
                }
                println(odgovori.size)
                Handler().postDelayed({
                    if(odgovor != -1){ //ovo je da li je uopce odgovoreno
                        odgovoreno = true
                        if(odgovor != -2) //izmjena odnosno ako je -2 znaci da nista nije odgovoreno
                            listaOdgovora.performItemClick(listaOdgovora, odgovor, listaOdgovora.adapter.getItemId(odgovor))
                        listaOdgovora.isEnabled = false
                    }
                    else
                        listaOdgovora.isEnabled = true
                }, 1)
            }
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Greska pri pozivanju", Toast.LENGTH_SHORT)
        toast.show()
    }
}