package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.models.StringPovrat
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.stream.Collectors


class FragmentPredmeti : Fragment() {
    private lateinit var godina: Spinner
    private lateinit var predmet : Spinner
    private lateinit var grupa : Spinner
    private lateinit var dugmeUpis : Button
    private lateinit var kvizViewModel : KvizListViewModel
    private lateinit var predmetViewModel : PredmetViewModel
    private lateinit var grupaViewModel : GrupaViewModel
    private lateinit var myInflater : LayoutInflater
    private lateinit var sharedViewModel: SharedViewModel
    var predmetiZaGodinu = listOf<Predmet>()
    var grupeZaPredmet = listOf<Grupa>()

    var brojacPromjena = 0 //brojac promjena kada postavljam zapamcenu godinu predmet i grupu!!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.predmet_fragment, container, false)
        kvizViewModel = KvizListViewModel()
        predmetViewModel = PredmetViewModel()
        grupaViewModel = GrupaViewModel()

        var aktivnost = activity as MainActivity
        sharedViewModel = aktivnost.sharedViewModel

        myInflater=inflater

        godina = view.findViewById(R.id.odabirGodina)
        predmet = view.findViewById(R.id.odabirPredmet)
        grupa = view.findViewById(R.id.odabirGrupa)
        dugmeUpis = view.findViewById(R.id.dodajPredmetDugme)

        pozoviGodinu() //ovo poziva spiner za godinu

        dugmeUpis.setOnClickListener(){
            var nazivPredmeta: String = predmet.selectedItem.toString()
            var nazivGrupe: String = grupa.selectedItem.toString()

            grupaViewModel.setGrupaZaStudenta(
                    onSuccess = ::onSuccess,
                    onError = ::onError,
                    grupa = sharedViewModel.odabranaGrupaId
            )


            val porukaFragment = FragmentPoruka.newInstance()
            val args = Bundle()
            args.putString("poruka", "Uspješno ste upisani u grupu ${nazivGrupe} predmeta ${nazivPredmeta}!")
            porukaFragment.setArguments(args)
            openFragment(porukaFragment)

            sharedViewModel.odabranaGodina = godina.selectedItemPosition
            sharedViewModel.odabranaGrupa = 0
            sharedViewModel.odabraniPredmet = 0
        }
        return view;
    }
    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

    private fun openFragment(fragment: Fragment) {
        val aktivnost : MainActivity = activity as MainActivity
        val transaction = aktivnost.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun pozoviGodinu(){
        var godine = resources.getStringArray(R.array.godine)
        var spinerAdapter: ArrayAdapter<String> = ArrayAdapter(myInflater.context,android.R.layout.simple_list_item_1, godine)
        spinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        godina.adapter = spinerAdapter

        //postavljam zapamcenu godinu
        if(brojacPromjena == 0)
            godina.setSelection(sharedViewModel.odabranaGodina)

        godina.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                brojacPromjena ++
                sharedViewModel.odabranaGodina = position
                predmetViewModel.getPredmetiZaGodinu(
                        onSuccess = ::onSuccessP,
                        onError = ::onError,
                        godina = position+1
                )
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }

    fun pozoviPredmete(){
        var predmetiNaziv : List<String>
        var predmeti = predmetiZaGodinu
        predmetiNaziv = predmeti.stream().map { p: Predmet -> p.naziv }.collect(Collectors.toList())

        dugmeUpis.isEnabled = !predmetiNaziv.isEmpty()
        var izabraniPredmet: Int = 0
        var spinerPredmetiAdapter: ArrayAdapter<String> = ArrayAdapter(myInflater.context,android.R.layout.simple_list_item_1, predmetiNaziv)
        spinerPredmetiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        predmet.adapter = spinerPredmetiAdapter

        //postavljam zapamceni predmet

        predmet.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                brojacPromjena++
                sharedViewModel.odabraniPredmet = position
                izabraniPredmet = predmeti[position].id
                grupaViewModel.getGrupeZaPredmet(
                        onSuccess = ::onSuccessG,
                        onError = ::onError,
                        predmet = izabraniPredmet
                )
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
        if(izabraniPredmet == 0) {
            grupeZaPredmet = mutableListOf()
            pozoviGrupe()
        }
    }

    fun pozoviGrupe(){
        var grupe = grupeZaPredmet
        var grupeNaziv = grupe.stream().map{ g: Grupa -> g.naziv}.collect(Collectors.toList())

        dugmeUpis.isEnabled = !grupeNaziv.isEmpty()

        var spinerGrupeAdapter = ArrayAdapter(myInflater.context,android.R.layout.simple_list_item_1, grupeNaziv)
        spinerGrupeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        grupa.adapter = spinerGrupeAdapter
        //postavljam zapamcenu grupu

        grupa.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                sharedViewModel.odabranaGrupaId = grupeZaPredmet[position].id
                sharedViewModel.odabranaGrupa = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }
    fun onSuccessP(predmeti:List<Predmet>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                predmetiZaGodinu = predmeti
                pozoviPredmete()

                if(brojacPromjena == 1)
                    predmet.setSelection(sharedViewModel.odabraniPredmet)
            }
        }
    }

    fun onSuccessG(grupe:List<Grupa>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                grupeZaPredmet = grupe
                pozoviGrupe()

                if(brojacPromjena == 2)
                    grupa.setSelection(sharedViewModel.odabranaGrupa)
            }
        }
    }

    fun onSuccess(potvrda: Boolean){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){

            }
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Greska pri pozivanju", Toast.LENGTH_SHORT)
        toast.show()
    }

}