package ba.etf.rma21.projekat.data.staticData

import ba.etf.rma21.projekat.data.models.PitanjeKviz

fun getAllPitanjeKviz() : List<PitanjeKviz>{
    return listOf(
            PitanjeKviz("Pitanje 1", "Kviz 1", -1),
            PitanjeKviz("Pitanje 2", "Kviz 1", -1),
            PitanjeKviz("Pitanje 3", "Kviz 1", -1),

            PitanjeKviz("Pitanje 4", "Kviz IF1 1", -1),
            PitanjeKviz("Pitanje 5", "Kviz IF1 1", -1),
            PitanjeKviz("Pitanje 6", "Kviz IF1 1", -1),

            PitanjeKviz("Pitanje 7", "Kviz DM 2", -1),
            PitanjeKviz("Pitanje 8", "Kviz DM 2", -1),
            PitanjeKviz("Pitanje 9", "Kviz DM 2", -1),

            PitanjeKviz("Pitanje 10", "Kviz IF1 2", -1),
            PitanjeKviz("Pitanje 11", "Kviz IF1 2", -1),
            PitanjeKviz("Pitanje 12", "Kviz IF1 2", -1),

            PitanjeKviz("Pitanje 13", "Kviz 2", -1),
            PitanjeKviz("Pitanje 14", "Kviz 2", -1),
            PitanjeKviz("Pitanje 15", "Kviz 2", -1),

            PitanjeKviz("Pitanje 16", "Kviz OBP 3", -1),
            PitanjeKviz("Pitanje 17", "Kviz OBP 3", -1),
            PitanjeKviz("Pitanje 18", "Kviz OBP 3", -1)
    )
}