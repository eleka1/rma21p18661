package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {

    @Query("SELECT * FROM pitanje")
    suspend fun getAllPitanje(): List<Pitanje>

    @Query("DELETE FROM pitanje")
    suspend fun ocistiPitanje()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPitanje(vararg pitanje: Pitanje)
}