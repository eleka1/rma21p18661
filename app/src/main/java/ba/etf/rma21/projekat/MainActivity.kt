package ba.etf.rma21.projekat

import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPoruka
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import ba.etf.rma21.projekat.viewmodel.DBViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigation: BottomNavigationView
    val sharedViewModel = SharedViewModel() //ovo mi je potrebno za pamcenje posljednjeg odabranog na fragment predmeti i pamcenje klika na odgovorima
    val kvizViewModel = KvizListViewModel() //ovo koristim jer ce mi trebati promjena stanja kviza kada zavrsi
    val accountViewModel = AccountViewModel()

    //Listener za click
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

            R.id.kvizovi -> {
                val kvizoviFragment = FragmentKvizovi.newInstance()
                openFragment(kvizoviFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val predmetiFragment = FragmentPredmeti.newInstance()
                openFragment(predmetiFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

//        val uri: Uri? = intent.data
//        if(uri != null){
//            val parmas = uri.pathSegments
//            val hash = parmas[parmas.size-1]
//            AccountRepository.postaviHash(hash)
//        } //treba li else dodati???


        val playload = intent?.getStringExtra("payload")
        if(playload != null){
            this?.let {
                accountViewModel.postaviHash(
                        it,onSuccess = ::onSuccess,
                        onError = ::onError,
                        playload = playload
                )
            }
        }
        //ovdje moram hash postaviti da bude fino !!!


        //postavljam sve vezano za bottom navigaciju
        bottomNavigation= findViewById(R.id.bottomNav)
        var meni = bottomNavigation.menu
        meni.findItem(R.id.kvizovi).isVisible = true
        meni.findItem(R.id.predmeti).isVisible = true
        meni.findItem(R.id.predajKviz).isVisible = false
        meni.findItem(R.id.zaustaviKviz).isVisible = false
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        //Defaultni fragment
        bottomNavigation.selectedItemId= R.id.kvizovi
        val kvizoviFragment = FragmentKvizovi.newInstance()
        openFragment(kvizoviFragment)
    }
    //Funkcija za izmjenu fragmenta
    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    //na back klik mora mi se otvoriti fragment kvizovi
    override fun onBackPressed() {
        bottomNavigation.selectedItemId= R.id.kvizovi
        val kvizoviFragment = FragmentKvizovi.newInstance()
        openFragment(kvizoviFragment)
    }

    //treba mi za pokusaj da se promijeni menu kada sam u fragment pokusaj
    fun getBottomNavigation(): BottomNavigationView {
        return bottomNavigation;
    }

    fun onSuccess(tacno: Boolean){
//        var dbViewModel = DBViewModel()
//        AccountRepository.acHash = ""
//        this?.let {
//            dbViewModel.updateDB(
//                    it,onSuccess = ::onSuccessDB,
//                    onError = ::onError
//            )
//        }
    }
    fun onSuccessDB(tacno: Boolean){
    }
    fun onError() {

    }
}

