package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class OdgovorViewModel{

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun dajOdgovore(context: Context, onSuccess: (odgovori: List<Odgovor>) -> Unit,
                    onError: () -> Unit,
                    id: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.getOdgovoriKviz(id) //izmjena

            // Display result of the network request to the user
            when (result) {
                is List<Odgovor> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun predajOdgovore(context: Context, onSuccess: () -> Unit,
                    onError: () -> Unit,
                    id: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            OdgovorRepository.setContext(context)
            val result = OdgovorRepository.predajOdgovore(id) //izmjena

            // Display result of the network request to the user

                onSuccess?.invoke()
        }

    }

    fun dajOdgovore2(onSuccess: (odgovori: List<Odgovor>) -> Unit,
                    onError: () -> Unit,
                    id: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = OdgovorRepository.getOdgovoriKviz2(id) //izmjena

            // Display result of the network request to the user
            when (result) {
                is List<Odgovor> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun postaviOdgovor(context: Context, onSuccess: (postotak: Int) -> Unit,
                       onError: () -> Unit,
                       idKvizTaken: Int,
                       idPitanje: Int,
                       odgovor: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            OdgovorRepository.setContext(context) //izmjena
            val result = OdgovorRepository.postaviOdgovorKviz(idKvizTaken, idPitanje, odgovor)

            // Display result of the network request to the user
            when (result) {
                is Int -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    fun dajBrojBodova(onSuccess: (postotak: Int) -> Unit,
                       onError: () -> Unit,
                       kvizId: Int,
                       idKvizTaken: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = OdgovorRepository.dajBodove(kvizId, idKvizTaken)

            // Display result of the network request to the user
            when (result) {
                is Int -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

    //pokusaj neke funkcije
    fun dajOdgovorNaPitanjeOdgovoreno(onSuccess: (postotak: Int) -> Unit,
                                      onError: () -> Unit,
                                      idKvizTaken: Int,
                                      idPitanje: Int
    ){
        // Create a new coroutine on the UI thread
        var povrat = -1
        scope.launch {
            // Make the network call and suspend execution until it finishes
            val odgovori = OdgovorRepository.getOdgovoriKviz2(idKvizTaken)
            if (odgovori != null) {
                for (odgovor in odgovori) {
                    if (odgovor.pitanjeId == idPitanje)
                        povrat = odgovor.odgovoreno
                }
            }
        }
        when (povrat) {
            is Int -> onSuccess?.invoke(povrat)
            else -> onError?.invoke()
        }
    }

    fun getPredano(onSuccess: (predan: Boolean) -> Unit,
    onError: () -> Unit,
    idKviz: Int
    ){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = OdgovorRepository.getPredano(idKviz)

            // Display result of the network request to the user
            when (result) {
                is Boolean -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }

    }

}