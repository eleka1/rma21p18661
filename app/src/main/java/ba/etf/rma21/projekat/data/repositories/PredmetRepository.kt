package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.staticData.getAllPredmeti
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PredmetRepository {
    companion object {
        //dodano
        var mojiPredmeti: MutableList<Predmet> = mutableListOf()

        fun dodajMojPredmet(predmeti: List<Predmet>){
            // TODO: Implementirati
            mojiPredmeti.addAll(predmeti)
//            return emptyList()
        }

        fun getUpisani(): List<Predmet> {
            // TODO: Implementirati
            return mojiPredmeti
//            return emptyList()
        }

        suspend fun getAll():List<Predmet>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmeti()

                return@withContext response
            }
        }
        // TODO: Implementirati i ostale potrebne metode
    }

}