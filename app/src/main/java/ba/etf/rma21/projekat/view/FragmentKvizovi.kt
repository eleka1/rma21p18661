package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentKvizovi : Fragment() {
    private lateinit var kvizovi: RecyclerView
    private lateinit var spiner: Spinner
    private lateinit var kvizAdapter: KvizListAdapter
    private lateinit var kvizViewModel: KvizListViewModel
    private  lateinit var sharedViewModel: SharedViewModel
    private var pitanjeViewModel = PitanjeKvizViewModel()
    private var kvizici : List<Kviz> = listOf()
    var pozicija = 0 //pozicija kviza na koji se kliknulo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.kviz_fragment, container, false)
        kvizViewModel = (activity as MainActivity).kvizViewModel
        kvizovi = view.findViewById(R.id.listaKvizova)
        kvizovi.layoutManager = GridLayoutManager(activity, 2)


        //ovo mi treba da bi kada iz kviza idem back da bi radilo sve ok
        var aktivnost = activity as MainActivity
        val meni = aktivnost.getBottomNavigation().menu
        meni.findItem(R.id.kvizovi).isVisible = true
        meni.findItem(R.id.predmeti).isVisible = true
        meni.findItem(R.id.predajKviz).isVisible = false
        meni.findItem(R.id.zaustaviKviz).isVisible = false

        sharedViewModel = aktivnost.sharedViewModel //uzimam instancu od main-a

        kvizAdapter = KvizListAdapter(arrayListOf()) {kviz-> otvoriFragment(kviz)}
        kvizovi.adapter = kvizAdapter

        context?.let {
            kvizViewModel.getMyKvizes(
                    it,onSuccess = ::onSuccessDB,
                    onError = ::onError)
        }

        //sve za spiner!!!
        var filteri = resources.getStringArray(R.array.filteri)
        spiner = view.findViewById(R.id.filterKvizova)
        var spinerAdapter: ArrayAdapter<String> =
            ArrayAdapter(inflater.context, android.R.layout.simple_spinner_item, filteri)
        spinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spiner.adapter = spinerAdapter
        spiner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                pozicija = position
                if (filteri[position] == "Svi moji kvizovi") {
                    context?.let {
                        kvizViewModel.getMyKvizes(
                                it,onSuccess = ::onSuccessDB,
                                onError = ::onError)
                    }
                } else if (filteri[position] == "Svi kvizovi") {
                    kvizViewModel.getAll(
                            onSuccess = ::onSuccess,
                            onError = ::onError
                    )
                } else if (filteri[position] == "Urađeni kvizovi") {
                    kvizViewModel.getDone(
                            onSuccess = ::onSuccess,
                            onError = ::onError
                    )
                } else if (filteri[position] == "Budući kvizovi") {
                    kvizViewModel.getFuture(
                            onSuccess = ::onSuccess,
                            onError = ::onError
                    )
                } else if (filteri[position] == "Prošli kvizovi") {
                    kvizViewModel.getNotTaken(
                            onSuccess = ::onSuccess,
                            onError = ::onError
                    )
                } else {
                    context?.let {
                        kvizViewModel.getMyKvizes(
                                it,onSuccess = ::onSuccessDB,
                                onError = ::onError)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        return view;
    }
    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    fun onSuccessDB(kvizes:List<Kviz>){
        kvizAdapter.updateKvizes(kvizes)
    }

    fun onSuccess(kvizes:List<Kviz>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                kvizAdapter.updateKvizes(kvizes)
            }
        }
    }

    fun onSuccessP(pitanja:List<Pitanje>){
        GlobalScope.launch(Dispatchers.IO){
            withContext(Dispatchers.Main){
                sharedViewModel.postotak = 0
                val fragment = FragmentPokusaj.newInstance(pitanja)
                openFragment(fragment)
            }
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Greska pri pozivanju", Toast.LENGTH_SHORT)
        kvizici = listOf()
        toast.show()
    }

    //funkcija koja mi regulise da li se uopce treba otvoriti fragment i poziva openFragment
    fun otvoriFragment(kviz : Kviz){
        var uslov = true
//        if(kvizViewModel.getFuture().filter { k -> k.naziv.equals(kviz.naziv) }.size != 0)
//            uslov = false
        if(pozicija != 1 && pozicija != 3 && uslov) { //ukoliko nije Buduci kviz ili Svi kvizovi ili Buduci kvizovi
            sharedViewModel.kviz = kviz.naziv
            sharedViewModel.kvizId = kviz.id
            sharedViewModel.predan = kviz.predan
            context?.let {
                pitanjeViewModel.getPitanjaZaKviz(
                        it, onSuccess = ::onSuccessP,
                        onError = ::onError,
                        kviz = kviz.id
                )
            }
        }
    }

    private fun openFragment(fragment: Fragment) {
        var aktivnost = activity as MainActivity
        val transaction = aktivnost.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}