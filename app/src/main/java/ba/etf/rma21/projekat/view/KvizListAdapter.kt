package ba.etf.rma21.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import java.text.SimpleDateFormat
import java.util.*

class KvizListAdapter (
    private var kvizovi: List<Kviz>,
    private val onItemClicked: (kviz:Kviz) -> Unit
) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {

//    var predanKviz = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.kviz_layout, parent, false)
        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {

        holder.kvizNaziv.text = kvizovi[position].naziv;
        holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta
        holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta
        holder.trajanje.text = kvizovi[position].trajanje.toString() + "min"


        //bodovi su NaN ako nije izasao na ispit
        if(!kvizovi[position].osvojeniBodovi.isNaN())
            holder.bodovi.text = kvizovi[position].osvojeniBodovi.toString()
        else
            holder.bodovi.text = ""

        //izmjena da ispravnu boju stavi kroz filter!!!
        //ovo se pobrine da se postavi ispravna slika i datum
//        var datumString : String = kvizovi[position].datumRada?.let { getDate(it) }.toString()
        var datumString : String? = kvizovi[position].datumRada
        var statusMatch: String = "zelena"
        if(kvizovi[position].predan && kvizovi[position].datumRada != null) {
            statusMatch = "plava"
//            datumString = kvizovi[position].datumRada?.let { getDate(it) }.toString()
            datumString = kvizovi[position].datumRada
        }
        if(stringToDate(kvizovi[position].datumPocetka)!!.after(Date()) && kvizovi[position].osvojeniBodovi.isNaN()){
            statusMatch = "zuta"
//            datumString = getDate(kvizovi[position].datumPocetka)
            datumString = kvizovi[position].datumPocetka
        }
        if(kvizovi[position].datumKraj != null && stringToDate(kvizovi[position].datumKraj)!!.before(Date()) && kvizovi[position].osvojeniBodovi.isNaN()) {
            statusMatch = "crvena"
//            datumString = kvizovi[position].datumKraj?.let { getDate(it) }.toString()
            datumString = kvizovi[position].datumKraj
        }
        if(!kvizovi[position].predan && (kvizovi[position].datumKraj == null || stringToDate(kvizovi[position].datumKraj)!!.after(Date())) && stringToDate(kvizovi[position].datumPocetka)!!.before(Date()) && kvizovi[position].osvojeniBodovi == 0F) {
            statusMatch = "zelena"
//            datumString = kvizovi[position].datumKraj?.let { getDate(it) }.toString()
            datumString = kvizovi[position].datumKraj
        }

        //postavlja sliku koja je odabrana prije
        val context: Context = holder.status.getContext()
        var id: Int = context.getResources()
            .getIdentifier(statusMatch, "drawable", context.getPackageName())
        if (id===0) id=context.getResources()
            .getIdentifier("zelena", "drawable", context.getPackageName())

        holder.status.setImageResource(id)
        holder.datum.text = datumString;

        holder.itemView.setOnClickListener{
            onItemClicked(kvizovi[position])}

    }

    fun updateKvizes(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }
    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val status: ImageView = itemView.findViewById(R.id.status)
        val kvizNaziv: TextView = itemView.findViewById(R.id.nazivKviza)
        val nazivPredmeta: TextView = itemView.findViewById(R.id.nazivPredmeta)
        val datum: TextView = itemView.findViewById(R.id.datum)
        val bodovi: TextView = itemView.findViewById(R.id.bodovi)
        val trajanje: TextView = itemView.findViewById(R.id.trajanje)
    }
    fun getDate(datum : Date) : String {
    val date: String
    val formater = SimpleDateFormat("dd.MM.yyyy")
    date = formater.format(datum)
    return date
    }
    fun stringToDate(string: String?): Date?{
        if(string == null)
            return null
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.parse(string)
    }
}