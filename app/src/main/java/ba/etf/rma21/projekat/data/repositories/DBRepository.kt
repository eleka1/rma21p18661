package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Changed
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class DBRepository {

    companion object{

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun updateNow() :Boolean{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                val datum = Date()
                val formater = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                var date: String = formater.format(datum)
                var promjenjeno : Changed

                if(db!!.accountDao().getAllAccount() != null && db!!.accountDao().getAllAccount().isNotEmpty() ) {
                        promjenjeno = ApiAdapter.retrofit.getUpdateTF(AccountRepository.acHash, db!!.accountDao().getAllAccount()[0].lastUpdate)
                } else {
                    promjenjeno = Changed(true)
                }

                var grupe = PredmetIGrupaRepository.getUpisaneGrupe()
//                if (grupe.size == 0)
//                    promjenjeno = Changed(true)

                AccountRepository.setContext(context)

                if(promjenjeno.changed) {
                    db!!.accountDao().ocistiAccount()
                    db!!.accountDao().insertAccount(Account(1, AccountRepository.acHash, date))
                    izbrisiSveBaza()
                    dodajSveBaza()
                    return@withContext true
                }
                return@withContext false
            }
        }

        suspend fun izbrisiSveBaza(){
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                db!!.kvizDao().ocistiKviz()
                db!!.grupaDao().ocistiGrupa()
//                db!!.odgovorDao().ocistiOdgovor()
                db!!.pitanjeDao().ocistiPitanje()
                db!!.predmetDao().ocistiPredmet()
                return@withContext
            }
        }

        suspend fun dodajSveBaza(){
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                PredmetIGrupaRepository.setContext(context)
                PitanjeKvizRepository.setContext(context)
                OdgovorRepository.setContext(context)


                var grupe = PredmetIGrupaRepository.getUpisaneGrupe()
                if(grupe != null) {
                    for (grupa in grupe)
                        db!!.grupaDao().insertGrupa(grupa)
                }

                var predmeti = PredmetIGrupaRepository.getMyPredmeti()
                if (predmeti != null) {
                    for (predmet in predmeti)
                        db!!.predmetDao().insertPredmet(predmet)
                }



                var kvizovi = KvizRepository.getMyKvizes2()
                var br = 1 // promjena sa 0 na 1!!
                var bro=1
                if (kvizovi != null) {
                    for(kviz in kvizovi) {
                        db!!.kvizDao().insertKviz(kviz)
                        var pitanja = PitanjeKvizRepository.getPitanja2(kviz.id)
                        if (pitanja != null) {
                            for(pitanje in pitanja) {
                                pitanje.idBaza = br
                                br++
                                pitanje.idKviza = kviz.id
                                db!!.pitanjeDao().insertPitanje(pitanje)
                            }
                        }
//                        var odgovori = OdgovorRepository.getOdgovoriKviz2(kviz.id)
//                        if (odgovori != null) {
//                            for (odgovor in odgovori){
//                                odgovor.id = bro
//                                bro++
//                                db!!.odgovorDao().insertOdgovor(odgovor)
//                            }
//                        }
                    }

                }
                return@withContext
            }
        }

    }

}