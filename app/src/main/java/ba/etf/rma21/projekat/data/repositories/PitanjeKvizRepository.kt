package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz
import ba.etf.rma21.projekat.data.staticData.getAllPitanja
import ba.etf.rma21.projekat.data.staticData.getAllPitanjeKviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository {
    companion object{

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPitanja2(idKviza:Int):List<Pitanje> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getPitanjaZaKviz(idKviza)
                return@withContext response
            }
        }

        suspend fun getPitanja(idKviza:Int):List<Pitanje> {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var povrat1 = db!!.pitanjeDao().getAllPitanje()
                povrat1 = povrat1.filter { p -> p.idKviza == idKviza }
                var povrat = mutableListOf<Pitanje>()
                for(p in povrat1){
                    if(povrat.filter { pi -> pi.id == p.id && pi.idKviza == p.idKviza }.isEmpty())
                        povrat.add(p)
                }
                return@withContext povrat
            }
        }



//        var listaPitanjaKviz = getAllPitanjeKviz()

//        fun getPitanja(nazivKviza:String,nazivPredmeta:String):List<Pitanje>{
//            //todo Implementirati metodu da ispravno vraća rezultat
//            var listaPitanjeKvizFiltrirano = listaPitanjaKviz.filter { p:PitanjeKviz -> p.kviz.equals(nazivKviza) }
//            return getAllPitanja().filter { p:Pitanje -> listaPitanjeKvizFiltrirano.filter { pk:PitanjeKviz -> pk.naziv.equals(p.naziv) }.isNotEmpty() }
//        }
//        fun getPitanjeKviz(nazivPitanja : String): PitanjeKviz {
////            return listaPitanjaKviz.filter { p:PitanjeKviz -> p.naziv.equals(nazivPitanja) }[0]
//        }
//
//        fun setPitanjeKviz(nazivPitanja : String, odgovorKorisnika:Int){
////            listaPitanjaKviz.filter { p:PitanjeKviz -> p.naziv.equals(nazivPitanja) }[0].odgovor = odgovorKorisnika
//        }

    }
}