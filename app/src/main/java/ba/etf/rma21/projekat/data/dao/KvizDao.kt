package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDao {

    @Query("SELECT * FROM kviz")
    suspend fun getAllKviz(): List<Kviz>

    @Query("DELETE FROM kviz")
    suspend fun ocistiKviz()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertKviz(vararg kviz: Kviz)
}