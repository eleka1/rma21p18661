package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.models.StringPovrat
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GrupaViewModel () {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun dodajMojeGrupe(grupe: List<Grupa>){
        // TODO: Implementirati
        GrupaRepository.dodajMojeGrupe(grupe)
//            return emptyList()
    }

    fun dajMojeGrupe(): List<Grupa> {
        return GrupaRepository.dajMojeGrupe()
    }

    fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa>{
        return GrupaRepository.getGroupsByPredmet(nazivPredmeta)
    }

    fun getGrupeZaPredmet(onSuccess: (grupe: List<Grupa>) -> Unit,
                            onError: () -> Unit, predmet: Int){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = PredmetIGrupaRepository.getGrupeZaPredmet(predmet)

            // Display result of the network request to the user
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }

    fun setGrupaZaStudenta(onSuccess: (message: Boolean) -> Unit,
                          onError: () -> Unit, grupa: Int){
        // Create a new coroutine on the UI thread
        scope.launch{
            // Make the network call and suspend execution until it finishes
            val result = PredmetIGrupaRepository.upisiUGrupu(grupa)

            // Display result of the network request to the user
            when (result) {
                is Boolean -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
}