package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository {
    companion object {

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        //prepravljana
        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var zapoceti = ApiAdapter.retrofit.getTakenKvizes(hash)
                var povrat: KvizTaken? = null
                for(kvizTaken in zapoceti){
                    if(kvizTaken.KvizId == idKviza){
                        povrat = kvizTaken
                        break
                    }
                }
                if (povrat == null)
                    povrat = ApiAdapter.retrofit.zapocniKviz(hash, idKviza)
                return@withContext povrat
            }
        }


        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getTakenKvizes(hash)
                if(response.isEmpty())
                    return@withContext null
                return@withContext response
            }
        }
    }

}