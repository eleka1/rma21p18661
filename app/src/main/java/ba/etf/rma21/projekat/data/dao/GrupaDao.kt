package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDao {

    @Query("SELECT * FROM grupa")
    suspend fun getAllGrupa(): List<Grupa>

    @Query("DELETE FROM grupa")
    suspend fun ocistiGrupa()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGrupa(vararg grupa: Grupa)

}