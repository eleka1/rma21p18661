package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.OdgovorenoPitanje
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class OdgovorRepository {
    companion object {

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getOdgovoriKviz2(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var kvizoviPokrenuti = TakeKvizRepository.getPocetiKvizovi()

                var kvizId = 0
                if (kvizoviPokrenuti != null) {
                    for(kt in kvizoviPokrenuti){
                        if(kt.KvizId == idKviza) {
                            kvizId = kt.id
                        }
                    }
                }
                var response = ApiAdapter.retrofit.getOdgovor(hash, kvizId)
                for(k in response){
                    k.kvizId = idKviza
                }
                return@withContext response
            }
        }

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var response1 = db!!.odgovorDao().getAllOdgovor()
                var response = mutableListOf<Odgovor>()
                for(o in response1){
                    if(o.kvizId == idKviza)
                        response.add(o)
                }

                return@withContext response
            }
        }

        suspend fun izracunajBodove(idKviza: Int): Int{
            return withContext(Dispatchers.IO) {
                var ukupniBodovi = 0
                var db = AppDatabase.getInstance(context)
                var odgovori = db!!.odgovorDao().getAllOdgovor()
                var pitanja = db!!.pitanjeDao().getAllPitanje()
                pitanja = pitanja.filter { p -> p.idKviza == idKviza }
                var brojPitanja = pitanja.size
                var brojTacnih = 0
                for(pitanje in pitanja){
                    if (odgovori != null) {
                        for(odgovorSvi in odgovori){
                            if(pitanje.id == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno)
                                brojTacnih++
                        }
                    }
                }
                ukupniBodovi = (brojTacnih*100.0/brojPitanja).roundToInt()
                return@withContext ukupniBodovi
            }
        }

        //provjeri ispravnost!!
        suspend fun predajOdgovore(idKviz: Int){
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getAllKviz()
                for(k in kvizovi)
                    if(k.id == idKviz){
                        k.predan = true
                        val date: String
                        val datum = Date()
                        val formater = SimpleDateFormat("dd.MM.yyyy")
                        date = formater.format(datum)
                        k.datumRada = date
                        k.osvojeniBodovi = izracunajBodove(idKviz).toFloat()
                        db!!.kvizDao().insertKviz(k)
                        break
                    }
                var response1 = db!!.odgovorDao().getAllOdgovor()
                var response = mutableListOf<Odgovor>()
                for(o in response1){
                    if(o.kvizId == idKviz)
                        response.add(o)
                }
                var pitanja = db!!.pitanjeDao().getAllPitanje()
                for(o in response){
                    var pomocna = pitanja.filter { p -> p.idBaza==o.pitanjeId }.first().id//izmjena
                    postaviOdgovorKviz2(o.kvizTakenid, pomocna, o.odgovoreno)//izmjena
                }
//                DBRepository.updateNow()
                return@withContext
            }
        }

        suspend fun getPredano(idKviza: Int): Boolean {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var kvizoviPokrenuti = TakeKvizRepository.getPocetiKvizovi()

                var kvizId = 0
                if (kvizoviPokrenuti != null) {
                    for(kt in kvizoviPokrenuti){
                        if(kt.KvizId == idKviza) {
                            kvizId = kt.id
                        }
                    }
                }
                var odgovori = ApiAdapter.retrofit.getOdgovor(hash, kvizId)
                var pitanja = PitanjeKvizRepository.getPitanja(idKviza)
                var povrat = false
                if(odgovori.size == pitanja.size)
                    povrat = true
                return@withContext povrat
            }
        }

        suspend fun dajBodove(kvizId: Int, idKvizTaken: Int): Int?{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var ukupniBodovi = 0
                var odgovori = ApiAdapter.retrofit.getOdgovor(hash, idKvizTaken)
                var pitanja = PitanjeKvizRepository.getPitanja(kvizId)
                var brojPitanja = pitanja.size
                var brojTacnih = 0
                for(pitanje in pitanja){
                    if (odgovori != null) {
                        for(odgovorSvi in odgovori){
                            if(pitanje.id == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno)
                                brojTacnih++
                        }
                    }
                }
                ukupniBodovi = (brojTacnih*100.0/brojPitanja).roundToInt()
                return@withContext ukupniBodovi
            }
        }

        suspend fun postaviOdgovorKviz2(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var kvizoviPokrenuti = TakeKvizRepository.getPocetiKvizovi()

                var kvizId = 0
                if (kvizoviPokrenuti != null) {
                    for(kt in kvizoviPokrenuti){
                        if(kt.id == idKvizTaken) {
                            kvizId = kt.KvizId
                        }
                    }
                }
                var ukupniBodovi = -1
                var odgovori = ApiAdapter.retrofit.getOdgovor(hash, idKvizTaken)
                var pitanja = PitanjeKvizRepository.getPitanja(kvizId)
                var brojPitanja = pitanja.size
                var tacno = false
                var brojTacnih = 0
                var bodovi = 0
                for(pitanje in pitanja){
                    if (odgovori != null) {
                        if(pitanje.id == idPitanje && pitanje.tacan == odgovor) { //izmjena
                            tacno = true
                            brojTacnih++
                        }
                        for(odgovorSvi in odgovori){
                            if(pitanje.id == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno)
                                brojTacnih++
                        }
                    }
                }
                if(tacno)
                    bodovi = (100.0/brojPitanja).roundToInt()
                ukupniBodovi = (brojTacnih*100.0/brojPitanja).roundToInt()

                var odgovoreno = OdgovorenoPitanje(odgovor, idPitanje, ukupniBodovi)
                var response = ApiAdapter.retrofit.setOdgovor(hash, idKvizTaken, odgovoreno)
                return@withContext ukupniBodovi
            }
        }

        //dovrsi
        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                val odgovoriSvi = db!!.odgovorDao().getAllOdgovor()
                var pitanja = db!!.pitanjeDao().getAllPitanje()

                var odgovori : MutableList<Odgovor> = mutableListOf()
                for(odgovor in odgovoriSvi){
                    if(odgovor.kvizTakenid == idKvizTaken)
                        odgovori.add(odgovor)
                }


                var tacno = false
                var upisi = true
                var brojTacnih = 0
                for(pitanje in pitanja){
                    if (odgovori != null) {
                        if(pitanje.idBaza == idPitanje && pitanje.tacan == odgovor) { //izmjena!!!!
                            if(odgovoriSvi.filter { o -> o.pitanjeId == idPitanje && o.kvizTakenid == idKvizTaken}.isNotEmpty())
                                upisi = false
                            else {
                                tacno = true
                                brojTacnih++
                            }
                        }
                        for(odgovorSvi in odgovori){
                            if(pitanje.idBaza == odgovorSvi.pitanjeId && pitanje.tacan == odgovorSvi.odgovoreno) //opet glupa izmjena
                                brojTacnih++
                        }
                    }
                }
                var pitanje : Pitanje? = null
                for(p in pitanja){
                    if(idPitanje == p.idBaza){ //izmjena
                        pitanje = p
                        break
                    }
                }
                pitanja = pitanja.filter { p -> p.idKviza == pitanje!!.idKviza }
                var brojPitanja = pitanja.size
                println(brojPitanja)

                var ukupniBodovi = -1
                var bodovi = 0
                if(tacno)
                    bodovi = (100.0/brojPitanja).roundToInt()
                ukupniBodovi = (brojTacnih*100.0/brojPitanja).roundToInt()

                if(upisi) {
                    var o = Odgovor(odgovoriSvi.size + 1, odgovor, idKvizTaken, idPitanje, pitanje!!.idKviza)
                    AppDatabase.getInstance(context).odgovorDao().insertOdgovor(o)
                }
                return@withContext ukupniBodovi
            }
        }

    }


}