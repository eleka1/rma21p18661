package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}

class KvizRepository {

    companion object {
        // TODO: Implementirati

        init {
            // TODO: Implementirati
        }

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getMyKvizes2(
        ) : List<Kviz>?{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getGrupeZaStudenta(hash)
                var povrat = mutableListOf<Kviz>()
                for(grupa in response) {
                    var responseKviz = ApiAdapter.retrofit.getKvizoveZaIdGrupe(grupa.id)
                    var imenaPredmeta = mutableListOf<String>()
                    for(kviz in responseKviz){
                        var predmet = ApiAdapter.retrofit.getPredmetZaId(grupa.predmetId).naziv
                        kviz.nazivGrupe = grupa.naziv // izmejna
                        if (kviz.nazivPredmeta == null)
                            kviz.nazivPredmeta = predmet
                        else
                            kviz.nazivPredmeta += ", " + predmet
                        povrat.add(kviz)
                    }
                }
                val sviKvizTaken = ApiAdapter.retrofit.getTakenKvizes(AccountRepository.getHash())
                for(kvizTaken in sviKvizTaken) {
                    for(kviz in povrat) {
                        if(kvizTaken.KvizId == kviz.id) {
                            //izmjena
                            var pitanja = PitanjeKvizRepository.getPitanja2(kviz.id) //izmjena //jos jedna izmjena
                            var odgovori = OdgovorRepository.getOdgovoriKviz2(kviz.id) //izmejna //jos jedna izmjena
                            kviz.datumRada = kvizTaken.datumRada
                            kviz.osvojeniBodovi = kvizTaken.osvojeniBodovi.toFloat() //mozda treba int ko zna
                            if(odgovori!=null && pitanja.size == odgovori.size)
                                kviz.predan = true //izmjena
                        }
                    }
                }
                return@withContext povrat
            }
        }


        suspend fun getDone(): List<Kviz>? {
            // TODO: Implementirati
            return withContext(Dispatchers.IO) {
                var response = getMyKvizes()?.filter { k: Kviz -> !k.osvojeniBodovi.isNaN()}
                return@withContext response
            }
        }

        fun stringToDate(string: String): Date?{
            if(string == null)
                return null
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.parse(string)
        }

        suspend fun getFuture(): List<Kviz>? {
            // TODO: Implementirati
            return withContext(Dispatchers.IO) {
                var response = getMyKvizes()?.filter { k: Kviz -> stringToDate(k.datumPocetka)!!.after(Date()) && k.osvojeniBodovi.isNaN() }
//                var response = getMyKvizes()?.filter { k: Kviz -> k.datumPocetka.after(Date()) && k.osvojeniBodovi.isNaN() }
                return@withContext response
            }
        }

        suspend fun getNotTaken(): List<Kviz>? {
            // TODO: Implementirati
            return withContext(Dispatchers.IO) {
                var response = getMyKvizes()?.filter { k: Kviz -> k.datumKraj!=null && stringToDate(k.datumKraj)!!.before(Date()) && k.osvojeniBodovi.isNaN() }
//                var response = getMyKvizes()?.filter { k: Kviz -> k.datumKraj!=null && k.datumKraj.before(Date()) && k.osvojeniBodovi.isNaN() }
                return@withContext response
            }
        }

        suspend fun getAll() : List<Kviz>?
        {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllKvizes()
                var povrat = mutableListOf<Kviz>()
                for(kviz in response) {
                    var responseGrupe = ApiAdapter.retrofit.getGrupaZaKviz(kviz.id)
                    var imenaPredmeta = mutableListOf<String>()
                    for(grupa in responseGrupe){
                        var predmet = ApiAdapter.retrofit.getPredmetZaId(grupa.predmetId).naziv
                        if(!imenaPredmeta.contains(predmet)) {
                            if (kviz.nazivPredmeta == "" || kviz.nazivPredmeta == null)
                                kviz.nazivPredmeta = predmet
                            else
                                kviz.nazivPredmeta += ", " + predmet
                            imenaPredmeta.add(predmet)
                        }
                        kviz.nazivGrupe = grupa.naziv
                    }
                    povrat.add(kviz)
                }
                val sviKvizTaken = ApiAdapter.retrofit.getTakenKvizes(AccountRepository.getHash())
                for(kvizTaken in sviKvizTaken) {
                    for(kviz in povrat) {
                        if(kvizTaken.KvizId == kviz.id) {
                            kviz.datumRada = kvizTaken.datumRada
                            kviz.osvojeniBodovi = kvizTaken.osvojeniBodovi.toFloat() //mozda treba int ko zna
                        }
                    }
                }
                if(povrat.isEmpty()) // izmjena
                    return@withContext null
                return@withContext povrat
            }
        }

        suspend fun getById(id: Int): Kviz? { //da li je ovdje potrebna izmjena kada mi vrati nista
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizZaId(id)
                return@withContext response
            }
        }

        suspend fun getUpisani(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var grupe = ApiAdapter.retrofit.getGrupeZaStudenta(hash)
                var response = mutableListOf<Kviz>()
                for(grupa in grupe) {
                    response.addAll(ApiAdapter.retrofit.getKvizoveZaIdGrupe(grupa.id))
                }
                return@withContext response
            }
        }

        suspend fun getMyKvizes() : List<Kviz> {
            return withContext(Dispatchers.IO) {
                DBRepository.setContext(context)
                DBRepository.updateNow()
                var db = AppDatabase.getInstance(context)
                var kvizes = db!!.kvizDao().getAllKviz()
                return@withContext kvizes
            }
        }

        suspend fun dodajMojKviz(kviz: Kviz) : String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.kvizDao().insertKviz(kviz)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }

    }
}