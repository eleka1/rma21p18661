package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class AccountRepository {

    companion object{

        private lateinit var context: Context
        fun setContext(_context:Context){
            context=_context
        }

        var acHash: String = "da5a0b6d-96d7-43a1-b25b-51bb5860ee73"

//        fun postaviHash(acHash:String):Boolean{
//            this.acHash = acHash
//            return true
//        }

        fun getHash():String{
            return acHash
        }


        suspend fun postaviHash(playload : String): Boolean{
            return withContext(Dispatchers.IO) {
                acHash = playload
                val datum = Date()
                val formater = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                var date: String = formater.format(datum)
                var db = AppDatabase.getInstance(context)
                db!!.accountDao().ocistiAccount()
                db!!.accountDao().insertAccount(Account(1, playload, date))//izmjena
                db!!.kvizDao().ocistiKviz()
                db!!.grupaDao().ocistiGrupa()
                db!!.odgovorDao().ocistiOdgovor()
                db!!.pitanjeDao().ocistiPitanje()
                db!!.predmetDao().ocistiPredmet()

                return@withContext true
            }
        }
    }
}