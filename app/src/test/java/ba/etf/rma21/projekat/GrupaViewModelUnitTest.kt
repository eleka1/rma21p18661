//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
//import org.hamcrest.CoreMatchers
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers
//import org.junit.Assert
//import org.junit.Test
//
//class GrupaViewModelUnitTest {
//    @Test
//    fun testGetGroupsByPredmet(){
//        var grupaViewModel: GrupaViewModel = GrupaViewModel()
//
//        val grupeIM1 = grupaViewModel.getGroupsByPredmet("IM1")
//        Assert.assertEquals(grupeIM1.size, 2)
//        MatcherAssert.assertThat(grupeIM1, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IM1"))))
//        MatcherAssert.assertThat(grupeIM1, Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("IM1G1"))))
//        MatcherAssert.assertThat(grupeIM1, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IF1")))))
//
//        val grupeDM = grupaViewModel.getGroupsByPredmet("DM")
//        Assert.assertEquals(grupeDM.size, 2)
//        MatcherAssert.assertThat(grupeDM, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM"))))
//        MatcherAssert.assertThat(grupeDM, Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("DMG1"))))
//        MatcherAssert.assertThat(grupeDM, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IF1")))))
//    }
//
//}