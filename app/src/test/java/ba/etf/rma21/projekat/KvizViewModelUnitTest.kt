//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import org.hamcrest.CoreMatchers
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers
//import org.junit.After
//import org.junit.Assert
//import org.junit.BeforeClass
//import org.junit.Test
//
//class KvizViewModelUnitTest {
//
//    var kvizViewModel: KvizListViewModel = KvizListViewModel()
//    var listaMojihKvizova = kvizViewModel.getAll().filter { k: Kviz -> (k.nazivPredmeta.equals("DM") && k.nazivGrupe.equals("DMG1"))
//            || (k.nazivPredmeta.equals("OBP") && k.nazivGrupe.equals("OBPG1"))} // nije buduci
//
//    @After
//    fun cleanUp(){
//        //ovo je za ciscenje nakon svakog testa jer dodajem stalno znaci moram i brisati!!!!
//        KvizRepository.mojiKvizovi.removeAll(listaMojihKvizova)
//    }
//
//
//    @Test
//    fun testGetAll(){
//
//        val kvizovi = kvizViewModel.getAll()
//        Assert.assertEquals(kvizovi.size, 9)
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IM1"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("NA")))))
//    }
//
//    @Test
//    fun testGetDone(){
//
//        kvizViewModel.dodajMojKviz(listaMojihKvizova)
//
//        val kvizovi = kvizViewModel.getDone()
//        Assert.assertEquals(kvizovi.size, 1)
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IF1"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM")))))
//
//
//    }
//
//    @Test
//    fun testNotTaken(){
//        kvizViewModel.dodajMojKviz(listaMojihKvizova)
//
//        val kvizovi = kvizViewModel.getNotTaken()
//        Assert.assertEquals(kvizovi.size, 1)
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivGrupe", CoreMatchers.`is`("DMG1"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("OBP")))))
//    }
//
//    @Test
//    fun testFuture(){
//        kvizViewModel.dodajMojKviz(listaMojihKvizova)
//
//        val kvizovi = kvizViewModel.getFuture()
//        Assert.assertEquals(kvizovi.size, 1)
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("OBP"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM")))))
//    }
//
//    @Test
//    fun testMyKvizes(){
//        kvizViewModel.dodajMojKviz(listaMojihKvizova)
//        //kako ovo testirati
//        val kvizovi = kvizViewModel.getMyKvizes()
//        Assert.assertEquals(kvizovi.size, 3)
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM"))))
//        MatcherAssert.assertThat(kvizovi, Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivGrupe", CoreMatchers.`is`("DMG1"))))
//    }
//}