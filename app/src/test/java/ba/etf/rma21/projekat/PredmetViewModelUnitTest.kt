//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import ba.etf.rma21.projekat.viewmodel.PredmetViewModel
//import org.hamcrest.CoreMatchers
//import org.hamcrest.MatcherAssert
//import org.hamcrest.Matchers
//import org.junit.Assert
//import org.junit.Test
//
//class PredmetViewModelUnitTest {
//    @Test
//    fun testGetAll(){
//        var predmetViewModel: PredmetViewModel = PredmetViewModel()
//
//        val predmeti = predmetViewModel.getAll()
//        Assert.assertEquals(predmeti.size, 4)
//        MatcherAssert.assertThat(predmeti, Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("IM1"))))
//        MatcherAssert.assertThat(predmeti, Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("IF1"))))
//        MatcherAssert.assertThat(predmeti, Matchers.not(Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("NA")))))
//    }
//
//    @Test
//    fun testGetUpisani(){
//        var predmetViewModel: PredmetViewModel = PredmetViewModel()
//
//        val velicina: Int = predmetViewModel.getUpisani().size
//        var listaMojihPredmeta = predmetViewModel.getAll().filter { p: Predmet -> p.naziv.equals("DM")}
//        predmetViewModel.dodajMojPredmet(listaMojihPredmeta)
//        val predmeti = predmetViewModel.getUpisani()
//        Assert.assertEquals(predmeti.size, velicina+1)
//        MatcherAssert.assertThat(predmeti, Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("DM"))))
//    }
//}